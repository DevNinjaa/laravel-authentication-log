<?php

namespace devninjaa\AuthenticationLog;

trait EventMap
{
    /**
     * The Authentication Log event / listener mappings.
     *
     * @var array
     */
    protected $events = [
        'Illuminate\Auth\Events\Login' => [
            'devninjaa\AuthenticationLog\Listeners\LogSuccessfulLogin',
        ],

        'Illuminate\Auth\Events\Logout' => [
            'devninjaa\AuthenticationLog\Listeners\LogSuccessfulLogout',
        ],
    ];
}
